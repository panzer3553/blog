class CommnentsController < ApplicationController
  before_action :set_commnent, only: [:show, :edit, :update, :destroy]

  # GET /commnents
  # GET /commnents.json
  def index
    @commnents = Commnent.all
  end

  # GET /commnents/1
  # GET /commnents/1.json
  def show
  end

  # GET /commnents/new
  def new
    @commnent = Commnent.new
  end

  # GET /commnents/1/edit
  def edit
  end

  # POST /commnents
  # POST /commnents.json
  def create
    @commnent = Commnent.new(commnent_params)

    respond_to do |format|
      if @commnent.save
        format.html { redirect_to @commnent, notice: 'Commnent was successfully created.' }
        format.json { render :show, status: :created, location: @commnent }
      else
        format.html { render :new }
        format.json { render json: @commnent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /commnents/1
  # PATCH/PUT /commnents/1.json
  def update
    respond_to do |format|
      if @commnent.update(commnent_params)
        format.html { redirect_to @commnent, notice: 'Commnent was successfully updated.' }
        format.json { render :show, status: :ok, location: @commnent }
      else
        format.html { render :edit }
        format.json { render json: @commnent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commnents/1
  # DELETE /commnents/1.json
  def destroy
    @commnent.destroy
    respond_to do |format|
      format.html { redirect_to commnents_url, notice: 'Commnent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commnent
      @commnent = Commnent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commnent_params
      params.require(:commnent).permit(:post_id, :body)
    end
end
